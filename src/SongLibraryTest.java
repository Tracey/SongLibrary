import static org.junit.Assert.*;
import java.awt.Rectangle;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import org.junit.Test;
import edu.cnu.cs.gooey.Gooey;
import edu.cnu.cs.gooey.GooeyDialog;
import edu.cnu.cs.gooey.GooeyFrame;

public class SongLibraryTest {

	/**
	 * Tests the initial frame setup components
	 */
	@Test
	public void testMainFrameComponents() {
		Gooey.capture(new GooeyFrame() {
			public void invoke() {
				SongLibrary.main(new String[] {});
			}

			public void test(JFrame frame) {
				// Tests that the frame is showing
				assertTrue("Frame not showing", frame.isShowing());

				// Tests that the Add button is enabled
				JButton addButton = Gooey.getButton(frame, "Add");
				assertEquals(true, addButton.isEnabled());

				// Tests that the delete button is disabled
				JButton deleteButton = Gooey.getButton(frame, "Delete");
				assertEquals(false, deleteButton.isEnabled());

				// Tests the title of the window
				assertEquals("SongLibrary", frame.getTitle());

				// Tests to see if the window is centered and packed on the
				// screen
				Rectangle actual = frame.getBounds();
				frame.setLocationRelativeTo(null);
				frame.pack();
				Rectangle exp = frame.getBounds();

				assertTrue(actual.getWidth() == exp.getWidth());
				assertTrue(actual.getHeight() == exp.getHeight());
				assertTrue(actual.x == exp.x);
				assertTrue(actual.y == exp.y);

				// Checks that the table is empty
				JTable checkT = Gooey.getComponent(frame, JTable.class);
				TableModel model = checkT.getModel();
				assertEquals(0, model.getRowCount());
				assertEquals(4, model.getColumnCount());

			}
		});
	}

	/**
	 * Tests closing the window from the menuBar Exit
	 */
	@Test
	public void testClosingWindow() {
		Gooey.capture(new GooeyFrame() {
			public void invoke() {
				SongLibrary.main(new String[] {});
			}

			public void test(JFrame frame) {
				// Tests getting the menuBar components
				JMenuBar menuBar = Gooey.getMenuBar(frame);
				JMenu songlib = Gooey.getMenu(menuBar, "SongLibrary");
				JMenuItem exit = Gooey.getMenu(songlib, "Exit");

				// Captures the exit dialog window upon exit click
				Gooey.capture(new GooeyDialog() {
					@Override
					public void invoke() {
						exit.doClick();
					}

					// Tests the exit dialog window
					public void test(JDialog dialog) {
						// Tests the title of the dialog window
						assertEquals("Incorrect title", "Select an Option", dialog.getTitle());
						// Tests the label of the window
						Gooey.getLabel(dialog, "Do you want to exit?");
						// Tests the button label and clicks the button
						JButton ok = Gooey.getButton(dialog, "Yes");
						ok.doClick();
						// Tests that the dialog window closes on button click
						assertFalse("Incorrect result", dialog.isShowing());
					}

				});

				// Tests that the frame closes on button click
				assertFalse("Incorrect Result", frame.isShowing());
			}

		});

	}

	/**
	 * Tests the About Dialog Box
	 */
	@Test
	public void testAboutDialog() {
		Gooey.capture(new GooeyFrame() {
			public void invoke() {
				SongLibrary.main(new String[] {});
			}

			public void test(JFrame frame) {
				// tests getting the menuBar components
				JMenuBar menuBar = Gooey.getMenuBar(frame);
				JMenu songlib = Gooey.getMenu(menuBar, "SongLibrary");
				JMenuItem about = Gooey.getMenu(songlib, "About");

				// Captures the about dialog window on button click
				Gooey.capture(new GooeyDialog() {
					@Override
					public void invoke() {
						about.doClick();
					}

					// Tests the about dialog window
					public void test(JDialog dialog) {
						// Tests the title of the about window
						assertEquals("Incorrect title", "Message", dialog.getTitle());
						// Tests the about window label
						Gooey.getLabel(dialog, "<html><hr><i>Song Library</i><br>by Tyler Tracey<hr></html>");
						// Tests the button label and clicks the button
						JButton ok = Gooey.getButton(dialog, "OK");
						ok.doClick();
						// Tests that the dialog window closes on button click
						assertFalse("Incorrect result", dialog.isShowing());
					}
				});
			}
		});
	}

	/**
	 * Tests loading a file with song information
	 */
	@Test
	public void testLoadingFileWithSongData() {
		Gooey.capture(new GooeyFrame() {
			public void invoke() {
				SongLibrary.main(new String[] {});
			}

			// Tests the menuBar components and invokes the open JMenuItem
			public void test(JFrame frame) {
				JMenuBar menuBar = Gooey.getMenuBar(frame);
				JMenu table = Gooey.getMenu(menuBar, "Table");
				JMenuItem open = Gooey.getMenu(table, "Open");
				Gooey.capture(new GooeyDialog() {
					@Override
					public void invoke() {
						open.doClick();
					}

					@Override
					public void test(JDialog d) {
						JFileChooser choose = Gooey.getComponent(d, JFileChooser.class);
						// Chosen file with song data
						File file = new File("/Users/traceys5/Desktop/song.txt");
						choose.setSelectedFile(file);
						// Selects the "open" button
						choose.approveSelection();

					}
				});

				// Tests that the table was filled with the information
				File file = new File("/Users/traceys5/Desktop/song.txt");
				JTable checkT = Gooey.getComponent(frame, JTable.class);
				TableModel model = checkT.getModel();
				assertEquals(1, model.getRowCount());
				assertEquals("Favorite Song", model.getValueAt(0, 0));
				assertEquals("Chance The Rapper", model.getValueAt(0, 1));
				assertEquals("Acid Rap", model.getValueAt(0, 2));
				assertEquals("2013", model.getValueAt(0, 3));

				// Tests that the title changed when a song was added
				assertEquals("SongLibrary [" + file.getAbsolutePath() + "]", frame.getTitle());

				// Tests that the delete button was enabled after adding songs
				// or remains disabled if no songs were added
				JButton delete = Gooey.getButton(frame, "Delete");
				if (model.getRowCount() > 0) {
					assertTrue(delete.isEnabled());
				} else {
					assertFalse(delete.isEnabled());
				}
			}

		});
	}

	/**
	 * Tests adding a song
	 */

	@Test
	public void testAddingASong() {
		Gooey.capture(new GooeyFrame() {
			public void invoke() {
				SongLibrary.main(new String[] {});
			}

			// Tests adding a new row to the table
			public void test(JFrame frame) {
				// Tests getting the table components
				JTable checkT = Gooey.getComponent(frame, JTable.class);
				TableModel model = checkT.getModel();

				// Tests clicking the add button
				JButton add = Gooey.getButton(frame, "Add");
				add.doClick();

				// Tests that there is an empty row at the beginning of the
				// table
				assertEquals(1, model.getRowCount());
				for (int i = 0; i < 4; i++) {
					assertEquals("", model.getValueAt(0, i));

				}

			}
		});
	}

}
