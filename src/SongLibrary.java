import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings({ "serial" })
public class SongLibrary extends JFrame {

	// JFrame
	public SongLibrary() {
		super("Song Library");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setSize(500, 250);
		setTitle("SongLibrary");
		setLayout(new BorderLayout());

		setResizable(false);

		addWindowListener(new WindowListener() {
			public void windowClosing(WindowEvent e) {
				JOptionPane closing = new JOptionPane();
				Object[] options = { "Yes", "No", "Cancel" };
				@SuppressWarnings("static-access")
				int input = closing.showOptionDialog(null, "Do you want to exit?", "Select an Option",
						closing.YES_NO_OPTION, closing.QUESTION_MESSAGE, null, options, options[0]);

				if (input == JOptionPane.YES_OPTION) {
					dispose();
				}
			}

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}
		});

		// Table Model
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Dates");
		model.addColumn("Coursework");
		model.addColumn("Album");
		model.addColumn("Year");

		// Center Table
		
		JTable mainTable = new JTable(model);
		mainTable.setPreferredScrollableViewportSize(new Dimension(500, 100));
		mainTable.setFillsViewportHeight(true);
		mainTable.setGridColor(Color.black);
		add(mainTable.getTableHeader(), BorderLayout.PAGE_START);
		add(new JScrollPane(mainTable));

		JButton delete = new JButton("Delete");
		delete.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				if (mainTable.getSelectedRow() != -1) {
					model.removeRow(mainTable.getSelectedRow());
				} else {
					JOptionPane noRow = new JOptionPane();
					noRow.showMessageDialog(null, "No row selected");
					noRow.setVisible(true);
				}

			}
		});
		delete.setEnabled(false);
		delete.setVisible(true);

		// JButtons
		JButton add = new JButton("Add");
		add.setVisible(true);
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.addRow(new Object[] { "", "", "", "" });
				delete.setEnabled(true);
			}
		});

		// East Box Creation
		Box east = Box.createVerticalBox();
		add(east, BorderLayout.EAST);
		east.add(add);
		east.add(delete);

		// Menu Bar Creation
		JMenuBar menuBar = new JMenuBar();
		JMenu songLib = new JMenu("SongLibrary");
		JMenu table = new JMenu("Table");

		// JMenuItem "About" Action Listener
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane aboutMe = new JOptionPane();
				aboutMe.showMessageDialog(null,
						new JLabel("<html><hr><i>Song Library</i><br>by Tyler Tracey<hr></html>"));
			}
		});

		// JMenu Item "Exit" Action Listener
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane exit = new JOptionPane();
				Object[] options = { "Yes", "No", "Cancel" };
				int input = exit.showOptionDialog(null, "Do you want to exit?", "Select an Option", exit.YES_NO_OPTION,
						exit.QUESTION_MESSAGE, null, options, options[0]);

				if (input == JOptionPane.YES_OPTION) {
					dispose();
				}
			}
		});

		// JMenuItem "New" Action Listener
		JMenuItem newB = new JMenuItem("New");
		newB.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane newB = new JOptionPane();
				Object[] options = { "Yes", "No", "Cancel" };
				int value = newB.showOptionDialog(null, "Do you want to clear all table data?", "Select an Option",
						newB.YES_NO_OPTION, newB.QUESTION_MESSAGE, null, options, options[0]);
				if (value == JOptionPane.YES_OPTION) {
					DefaultTableModel setnew = new DefaultTableModel();
					setnew.addColumn("Song");
					setnew.addColumn("Artist");
					setnew.addColumn("Album");
					setnew.addColumn("Year");
					mainTable.setModel(setnew);
					delete.setEnabled(false);
				}
			}
		});

		// JMenu Item "Open" Action Listener
		JMenuItem open = new JMenuItem("Open");
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JFileChooser openChoose = new JFileChooser();
						openChoose.setCurrentDirectory(new java.io.File("/Users/traceys5/Desktop/SongLibrary"));
						openChoose.setDialogTitle("Open");
						int value = openChoose.showOpenDialog(null);
						if (value == JFileChooser.APPROVE_OPTION) {
							try {
								File tmp = openChoose.getSelectedFile();
								FileReader Fread = new FileReader(tmp);
								@SuppressWarnings("resource")
								BufferedReader read = new BufferedReader(Fread);
								String str;
								while ((str = read.readLine()) != null) {
									String[] line = str.split(",");
									model.addRow(new Object[] { line[0], line[1], line[2], line[3] });
								}
								if (!(tmp.length() == 0)) {
									delete.setEnabled(true);
								} else {
									delete.setEnabled(false);
								}
								setTitle("SongLibrary [" + tmp.getAbsolutePath() + "]");

							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}

					}

				});
			}

		});

		// JMenuItem "Save Action Listener
		JMenuItem save = new JMenuItem("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JFileChooser saveChooser = new JFileChooser();
						saveChooser.setDialogTitle("Save");
						int result = saveChooser.showSaveDialog(SongLibrary.this);
						if (result == JFileChooser.APPROVE_OPTION) {
							try {
								File file = saveChooser.getSelectedFile();
								PrintWriter writer = new PrintWriter(file);
								for (int row = 0; row < mainTable.getRowCount(); row++) {
									if (row > 0) {
										writer.print("\n");
									}
									for (int col = 0; col < mainTable.getColumnCount(); col++) {
										writer.print(mainTable.getValueAt(row, col));
										if (col != mainTable.getColumnCount() - 1) {
											writer.print(",");
										}
									}
								}
								writer.close();

							} catch (FileNotFoundException e) {
								e.printStackTrace();

							}
						}
					}
				});

			}
		});

		setJMenuBar(menuBar);
		add(mainTable);
		menuBar.add(songLib);
		songLib.add(about);
		songLib.add(exit);
		menuBar.add(table);
		table.add(newB);
		table.add(open);
		table.add(save);

		
		setLocationRelativeTo(null);
		setVisible(true);

	}

	public static void main(String args[]) {
		SongLibrary x = new SongLibrary();
		x.setVisible(true);
	}

}
